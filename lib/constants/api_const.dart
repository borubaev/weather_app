class ApiConst {
  static String dataWeather(String? city) {
    return 'https://api.openweathermap.org/data/2.5/weather?q=$city&appid=41aa18abb8974c0ea27098038f6feb1b&lang=ru';
  }

  static String getIcon(String icon) {
    return 'https://openweathermap.org/img/wn/${icon}@4x.png';
  }

  static String latLongAddres(double lat, double long) {
    return 'https://api.openweathermap.org/data/2.5/onecall?lat=42.84&lon=76.06&exclude=hourly,daily,minutely&appid=41aa18abb8974c0ea27098038f6feb1b&lang=ru';
  }
}
