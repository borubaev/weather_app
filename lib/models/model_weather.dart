class WeatherData {
  WeatherData(
      {required this.id,
      required this.main,
      required this.description,
      required this.icon,
      required this.temp,
      required this.humidity,
      required this.wind,
      required this.date,
      required this.name});

  final int id;
  final String main;
  final String description;
  final String icon;
  final double temp;
  final int humidity;
  final dynamic wind;
  final int date;
  final String name;
}
