import 'package:flutter/material.dart';

class weatherItem extends StatelessWidget {
  const weatherItem({
    super.key,
    required this.text,
    required this.kmh,
    required this.imgUrl,
  });
  final String text;
  final String kmh;
  final String imgUrl;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          '$text',
          style: TextStyle(color: Colors.black54),
        ),
        const SizedBox(
          height: 8,
        ),
        Container(
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(
              Radius.circular(15),
            ),
            color: const Color.fromARGB(255, 187, 189, 189).withOpacity(.5),
          ),
          padding: EdgeInsets.all(15),
          height: 70,
          width: 70,
          child: Image.asset(
            imgUrl,
            fit: BoxFit.cover,
          ),
        ),
        const SizedBox(
          height: 8,
        ),
        Text(
          '$kmh',
          style: const TextStyle(fontWeight: FontWeight.bold),
        )
      ],
    );
  }
}
