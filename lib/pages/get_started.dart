import 'package:flutter/material.dart';
import 'package:weather_app/constants/appColors.dart';
import 'package:weather_app/pages/home.dart';
// import 'package:weather_app/pages/welcome.dart';

class Weather extends StatefulWidget {
  @override
  State<Weather> createState() => _MyWeather();
}

class _MyWeather extends State<Weather> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: Container(
        width: size.width,
        height: size.height,
        color: AppColors.primaryColor.withOpacity(0.5),
        child: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // Container(
            //   width: 300,
            //   height: 300,
            //   decoration: const BoxDecoration(
            //       image: DecorationImage(
            //           image: AssetImage('assets/get-started.png'),
            //           fit: BoxFit.cover),
            //       shape: BoxShape.circle,
            //       color: Colors.red),
            // ),
            Image.asset("assets/start.png"),
            const SizedBox(
              height: 50,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const HomePage()));
              },
              child: Container(
                height: 50,
                width: size.width * 0.7,
                decoration: BoxDecoration(
                  color: Colors.blue.withOpacity(0.7),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: const Center(
                    child: Text(
                  'Get Started',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                  ),
                )),
              ),
            )
          ],
        )),
      ),
    );
  }
}
