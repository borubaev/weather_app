import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:weather_app/constants/api_const.dart';
import 'package:weather_app/models/model_weather.dart';
import '../constants/appColors.dart';
import '../models/weatherItem.dart';

const List cities = <String>[
  'Osh',
  'Bishkek',
  'Naryn',
  'London',
  'Jalal-Abad',
  'Talas',
  'Batken',
];

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  WeatherData? weather;
  String formattedDate = '';
  bool isLoading = true;
  TextEditingController myController = TextEditingController();
  String search = '';

  Future<void> getLocation() async {
    LocationPermission permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.always &&
          permission == LocationPermission.whileInUse) {
        Position position = await Geolocator.getCurrentPosition();
        final dio = Dio();
        final result = await dio.get(
          ApiConst.latLongAddres(position.latitude, position.longitude),
        );
        if (result.statusCode == 200) {
          weather = WeatherData(
              id: result.data['current']['weather'][0]['id'],
              main: result.data['current']['weather'][0]['main'],
              description: result.data['current']['weather'][0]['description'],
              icon: result.data['current']['weather'][0]['icon'],
              temp: result.data['current']['temp'],
              humidity: result.data['current']['humidity'],
              wind: result.data['current']['wind_speed'],
              date: result.data['current']['dt'],
              name: result.data['timezone']);
        }
        setState(() {});
      }
    } else {
      Position position = await Geolocator.getCurrentPosition();
      print(position.latitude);
      print(position.longitude);
      final dio = Dio();
      final result = await dio.get(
        ApiConst.latLongAddres(position.latitude, position.longitude),
      );
      if (result.statusCode == 200) {
        weather = WeatherData(
            id: result.data['current']['weather'][0]['id'],
            main: result.data['current']['weather'][0]['main'],
            description: result.data['current']['weather'][0]['description'],
            icon: result.data['current']['weather'][0]['icon'],
            temp: result.data['current']['temp'],
            humidity: result.data['current']['humidity'],
            wind: result.data['current']['wind_speed'],
            date: result.data['current']['dt'],
            name: result.data['timezone']);
      }
      setState(() {});
    }
  }

  Future<void> fetchData([String? city]) async {
    Dio dio = Dio();
    // await Future.delayed(Duration(seconds: 3));
    final response = await dio.get(ApiConst.dataWeather(city ?? 'bishkek'));
    if (response.statusCode == 200) {
      weather = WeatherData(
          id: response.data['weather'][0]['id'],
          main: response.data['weather'][0]['main'],
          description: response.data['weather'][0]['description'],
          icon: response.data['weather'][0]['icon'],
          temp: response.data['main']['temp'],
          humidity: response.data['main']['humidity'],
          wind: response.data['wind']['speed'],
          date: response.data['dt'],
          name: response.data['name']);
    }

    setState(() {
      DateTime date = DateTime.fromMillisecondsSinceEpoch(weather!.date * 1000);
      var locale = const Locale('ru');
      initializeDateFormatting(locale.toString(), null);
      formattedDate = DateFormat('dd MMMM', locale.toString()).format(date);
    });
  }

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: false,
        backgroundColor: AppColors.primaryColor.withOpacity(.9),
        elevation: 0,
        title: Container(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            width: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  onPressed: () async {
                    await getLocation();
                    print('object');
                  },
                  icon: const Icon(
                    Icons.near_me,
                    size: 30,
                  ),
                ),
                IconButton(
                  onPressed: () async {
                    bottomNavbar();
                    print('select cities');
                  },
                  icon: const Icon(
                    Icons.location_city,
                    size: 30,
                  ),
                )
              ],
            )),
      ),
      body: Center(
        child: weather != null
            ? Container(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: 10,
                    ),
                    FittedBox(
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          '${weather!.name}',
                          style: const TextStyle(
                              fontSize: 40, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    Text(
                      'Сегодня, $formattedDate',
                      style: const TextStyle(fontSize: 16, color: Colors.grey),
                    ),
                    const SizedBox(
                      height: 70,
                    ),
                    Container(
                      width: double.infinity,
                      height: 200,
                      decoration: BoxDecoration(
                          color: AppColors.primaryColor.withOpacity(.8),
                          borderRadius: const BorderRadius.all(
                            Radius.circular(15),
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: const Color(0xff91bbfa).withOpacity(.5),
                              offset: Offset(0, 35),
                              spreadRadius: -6,
                              blurRadius: 10,
                            ),
                          ]),
                      child: Stack(
                        clipBehavior: Clip.none,
                        children: [
                          Positioned(
                            top: -70,
                            left: 0,
                            child: Image.network(
                              ApiConst.getIcon(weather!.icon),
                              width: 180,
                              height: 180,
                            ),
                          ),
                          Positioned(
                            bottom: 30,
                            left: 20,
                            child: Text(
                              '${weather!.description}',
                              style: const TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                              ),
                            ),
                          ),
                          Positioned(
                            top: 20,
                            right: 20,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: EdgeInsetsDirectional.all(4.0),
                                  child: Text(
                                    '${(weather!.temp - 273.15).toInt()}',
                                    style: const TextStyle(
                                        fontSize: 60,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white70),
                                  ),
                                ),
                                const Text(
                                  'o',
                                  style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white70),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 60,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        weatherItem(
                          text: 'Скорость ветра',
                          kmh: '${weather!.wind} km/h',
                          imgUrl: 'assets/windspeed.png',
                        ),
                        weatherItem(
                          text: 'Влажность',
                          kmh: "${weather!.humidity} %",
                          imgUrl: 'assets/humidity.png',
                        ),
                        weatherItem(
                          text: 'Температура',
                          kmh: '${(weather!.temp - 273.15).toInt()} / C',
                          imgUrl: 'assets/max-temp.png',
                        ),
                      ],
                    ),
                  ],
                ),
              )
            : const CircularProgressIndicator(),
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(right: 30, bottom: 30),
        child: FloatingActionButton(
          onPressed: () {
            dialogSearch(myController);
          },
          backgroundColor: AppColors.primaryColor,
          child: const Icon(
            Icons.search,
          ),
        ),
      ),
    );
  }

  void dialogSearch(TextEditingController myController) async {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) => Dialog(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 50,
                child: TextField(
                  controller: myController,
                  onChanged: (value) {
                    search = value;
                  },
                  decoration: const InputDecoration(
                    labelText: 'Поиск города...',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(7),
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 15),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  OutlinedButton(
                    onPressed: () {
                      setState(() {
                        weather = null;
                      });
                      Navigator.pop(context);
                      fetchData(search);
                      myController.clear();
                    },
                    child: Text('Поиск..'),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  OutlinedButton(
                    onPressed: () {
                      Navigator.pop(context);
                      search = '';
                    },
                    child: Text('Отмена'),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  void bottomNavbar() async {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: 500,
          color: AppColors.primaryColor,
          child: ListView.builder(
            itemCount: cities.length,
            itemBuilder: (context, index) {
              final city = cities[index];
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  child: ListTile(
                    onTap: () async {
                      Navigator.pop(context);
                      setState(() {
                        weather = null;
                      });
                      await fetchData(city);
                    },
                    title: Text('$city'),
                  ),
                ),
              );
            },
          ),
        );
      },
    );
  }
}
